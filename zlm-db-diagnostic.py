#!/usr/bin/python
# Check ZLM DB/ldap base consistency
# Author: Maer Melo
#         salalbr@gmail.com
####################################

import sys
import os.path
import re
from itertools import izip, tee, islice

def usage():
	print 'Usage: zlm-db-diagnostic.py <Mode: Bundle | Device>\
		<LDIF export file> <DB export file>'
	return

def check_args():
	if (len(sys.argv) != 4):
		usage()
		sys.exit(2)

	if os.path.isfile(sys.argv[2]) != True:
		print 'No such file: ' + sys.argv[2]
		usage()
		sys.exit(2)

	if os.path.isfile(sys.argv[3]) != True:
		print 'No such file: ' + sys.argv[3]
		usage()
		sys.exit(2)
		
def process_ldif(line):
	list = line.strip()
	line = re.sub(r'\W+', ' ', line)
	line = re.sub('[0-9]', ' ', line)
	line = re.sub('ll', 'will', line)
	return line.split()

# Main
check_args()
mode = sys.argv[1].lower
ldifFile = sys.argv[2]
csvFile = sys.argv[3]
ldap_uids = []
ldap_names = []
db_uids = []
db_names = []

# Process ldif file
i1 = open(ldifFile)
i2 = open(ldifFile)

for line1, line2, in izip(islice(i1,0, None, 3),\
				islice(i2, 1, None, 3)):
	ldap_uid, ldap_name = line1.split()[1],\
					line2.split()[1]
	#print ldap_dn, ldap_uid, ldap_name
	if ldap_uid not in ldap_uids:
		ldap_uids.append(ldap_uid)
		ldap_names.append(ldap_name)
	
# Process csv file
for line in open(csvFile):
	if not line.strip():
		continue
	else:
		if line.split()[0] == 'bundleguid' or\
		   line.split()[0] == 'deviceguid' or\
		   line.split()[0].startswith('(') == True or\
		   line.split()[0].startswith('------') == True:
		   	continue
		else:
			line = re.sub(r'\W+', ' ', line)
			csv_uid, csv_name = line.split()[0], line.split()[1] 
			#print csv_uid, csv_name
			if csv_uid not in db_uids:
				db_uids.append(csv_uid)
				db_names.append(csv_name)

# Check consistency - left outer join (eDir-DB)
print '---Devices not available in the database (Postgres)---'
for i in range(0, len(ldap_uids)):
	if ldap_uids[i] not in db_uids:
		print ldap_names[i], ' | ', ldap_uids[i]

# Check consistency - left outer join (eDir-DB)
print '---Devices not available in ldap directory (eDirectory)---'
for i in range(0, len(db_uids)):
	if db_uids[i] not in ldap_uids:
		print db_names[i], ' | ', db_uids[i]

'''
###Debugging code showing list contents
print ldap_uids
print ldap_names
print db_uids
print db_names
'''

'''
# Print out processing
sorted_list = sorted(dict.items(), key=lambda total: total[1], reverse=True)
for word, total in sorted_list:
	print "%20s: %d" % (word, total)
print '\n       # of elements:', len(sorted_list)
'''
